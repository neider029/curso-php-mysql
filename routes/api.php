<?php

declare(strict_types = 1);

use Pecee\SimpleRouter\SimpleRouter as Router;

Router::group(
    settings: ['prefix' => 'api'],
    callback: function (): void {
        Router::get(url:'/', callback: 'ApiController@__invoke');

        Router::group(
            settings: ['prefix' => 'products','namespace' => 'Product'],
            callback: function (): void {
                Router::post(url:'/', callback: 'CreateProductController@__invoke');
                Router::get(url:'/', callback: 'ListProductController@__invoke');
                Router::get(url:'/{id}', callback: 'GetProductController@__invoke');
                Router::put(url:'/{id}', callback: 'UpdateProductController@__invoke');
                Router::delete(url:'/{id}', callback: 'DeleteProductController@__invoke');
            }
        );
    }
);
