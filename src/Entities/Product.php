<?php

declare(strict_types = 1);

namespace App\Entities;

use App\Services\DBConnection;
use PDO;

final class Product
{
    private PDO $db;

    public function __construct()
    {
        $this->db = DBConnection::getConnection();

        $this->createTable();
    }

    private function createTable(): void
    {
        $sql = 'CREATE TABLE IF NOT EXISTS products (
            id INT AUTO_INCREMENT PRIMARY KEY,
            name VARCHAR(50) NOT NULL,
            description VARCHAR(255) NOT NULL,
            price DECIMAL(10, 2) NOT NULL,
            created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
        )';

        $this->db->exec(statement: $sql);
    }

    /**
     * @param array<string, mixed> $data
     * @return false|string
     */
    public function create(array $data): false|string
    {
        $sql = '
            INSERT INTO products (name, description, price)
            VALUES (:name, :description, :price)
        ';

        $stmt = $this->db->prepare(query: $sql);
        $stmt->execute(params: $data);

        return $this->db->lastInsertId();
    }

    /**
     * @return array<int, array<string, string>>
     */
    public function get(): array
    {
        $sql = 'SELECT * FROM products';
        $stmt = $this->db->prepare(query: $sql);
        $stmt->execute();

        return $stmt->fetchAll(mode: PDO::FETCH_ASSOC);
    }

    /**
     * @param int $id
     * @return array<string, string>|false
     */
    public function find(int $id): array|false
    {
        $sql = 'SELECT * FROM products WHERE id = :id';
        $stmt = $this->db->prepare(query: $sql);
        $stmt->execute(params: ['id' => $id]);

        return $stmt->fetch(mode: PDO::FETCH_ASSOC);
    }

    /**
     * @param array<string, mixed> $data
     * @return void
     */
    public function update(array $data): void
    {
        $sql = '
            UPDATE products
            SET name = :name, description = :description, price = :price
            WHERE id = :id
        ';

        $stmt = $this->db->prepare(query: $sql);
        $stmt->execute(params: $data);
    }

    public function delete(int $id): void
    {
        $sql = 'DELETE FROM products WHERE id = :id';
        $stmt = $this->db->prepare(query: $sql);
        $stmt->execute(params: ['id' => $id]);
    }
}
