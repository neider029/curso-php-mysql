<?php

declare(strict_types = 1);

use Pecee\Http\Input\InputHandler;
use Pecee\SimpleRouter\SimpleRouter as Router;
use Pecee\Http\Response;
use Pecee\Http\Request;

/**
 * @return \Pecee\Http\Response
 */
function response(): Response
{
    return Router::response();
}

/**
 * @return \Pecee\Http\Request
 */
function request(): Request
{
    return Router::request();
}

/**
 * @param string|null $index
 * @param mixed|null $defaultValue
 * @param array<string> $methods
 * @return array<string, mixed>|string|int|InputHandler
 */
function input(?string $index = null, mixed $defaultValue = null, ...$methods): array|string|int|InputHandler
{
    if ($index !== null) {
        return request()->getInputHandler()->value($index, $defaultValue, ...$methods);
    }

    return request()->getInputHandler();
}
