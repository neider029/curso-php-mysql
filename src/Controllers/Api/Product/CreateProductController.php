<?php

declare(strict_types = 1);

namespace App\Controllers\Api\Product;

use App\Entities\Product;
use App\Responses\JsonResponse;
use App\Requests\ProductRequest;

final class CreateProductController
{
    public function __invoke(): void
    {
        $product = new Product();
        $request = new ProductRequest();

        $productId = $product->create(
            data:$request->validated()
        );

        JsonResponse::response(
            data:$product->find(id: (int) $productId),
        );
    }
}
