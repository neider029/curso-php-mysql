<?php

declare(strict_types = 1);

namespace App\Controllers\Api\Product;

use App\Entities\Product;
use App\Responses\JsonResponse;

final class GetProductController
{
    public function __invoke(int $id): void
    {
        $product = new Product();
        $product = $product->find(id: $id);

        if (!$product) {
            JsonResponse::response(
                data: [
                    'message' => 'Producto no encontrado',
                ],
                httpCode: 404
            );
        }

        JsonResponse::response(data: $product);
    }
}
