<?php

declare(strict_types = 1);

namespace App\Controllers\Api\Product;

use App\Entities\Product;
use App\Responses\JsonResponse;

final class ListProductController
{
    public function __invoke(): void
    {
        $product = new Product();
        $products = $product->get();

        JsonResponse::response(
            data:[
                'products' => $products,
            ]
        );
    }
}
