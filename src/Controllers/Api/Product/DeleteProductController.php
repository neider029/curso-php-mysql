<?php

declare(strict_types = 1);

namespace App\Controllers\Api\Product;

use App\Entities\Product;
use App\Responses\JsonResponse;

final class DeleteProductController
{
    public function __invoke(int $id): void
    {
        $product = new Product();

        if (!$product->find(id: $id)) {
            JsonResponse::response(
                data: [
                    'message' => 'Producto no encontrado',
                ],
                httpCode: 404
            );
        }

        $product->delete(id:$id);

        JsonResponse::response();
    }
}
