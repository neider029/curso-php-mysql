<?php

declare(strict_types = 1);

namespace App\Controllers\Api\Product;

use App\Entities\Product;
use App\Requests\ProductRequest;
use App\Responses\JsonResponse;

final class UpdateProductController
{
    public function __invoke(int $id): void
    {
        $product = new Product();
        $request = new ProductRequest(id:$id);

        if (!$product->find(id: $id)) {
            JsonResponse::response(
                data: [
                    'message' => 'Producto no encontrado',
                ],
                httpCode: 404
            );
        }

        $product->update(
            data:
            [
                'id' => $id,
                ...$request->validated(),
            ]
        );

        JsonResponse::response(data: $product->find(id:$id));
    }
}
