<?php

declare(strict_types = 1);

namespace App\Rules;

use App\Services\DBConnection;

final class UniqueRule extends AbstractRule
{
    public function __construct(
        private readonly string $table,
        private readonly string $column,
        private readonly ?int $id,
        string $message
    ) {
        parent::__construct($message);
    }

    public function validate(mixed $value): bool
    {
        $query = "SELECT * FROM {$this->table} WHERE {$this->column} = :value";

        if ($this->id) {
            $query .= " AND id != {$this->id}";
        }
        $statement = DBConnection::getConnection()->prepare($query);
        $statement->bindValue(param:':value', value: $value);
        $statement->execute();

        return $statement->rowCount() === 0;
    }
}
