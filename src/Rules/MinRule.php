<?php

declare(strict_types = 1);

namespace App\Rules;

class MinRule extends AbstractRule
{
    public function __construct(private readonly int $length, string $message = '')
    {
        parent::__construct($message);
    }

    public function validate(mixed $value): bool
    {
        return strlen($value) >= $this->length;
    }
}
