<?php

declare(strict_types = 1);

namespace App\Rules;

abstract class AbstractRule
{
    public function __construct(protected readonly string $message = '')
    {
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    abstract public function validate(mixed $value): bool;
}
