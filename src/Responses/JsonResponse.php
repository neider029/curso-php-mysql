<?php

declare(strict_types = 1);

namespace App\Responses;

use http\Exception\InvalidArgumentException;

final class JsonResponse
{
    private const SUCCESS = 'success';

    private const ERROR = 'error';

    private static function statusByHttpCode(int $httpCode): string
    {
        if ($httpCode >= 400) {
            return self::ERROR;
        }

        return self::SUCCESS;
    }

    /**
     * @param array<string, mixed>|null $data
     * @return void
     */
    private static function success(?array $data = null): void
    {
        if (!$data) {
            response()
                ->httpCode(code: 204);
            exit;
        }

        response()
            ->httpCode(code: 200)
            ->json(
                value: [
                    'status' => self::SUCCESS,
                    'data'   => $data,
                ]
            );
    }

    private static function error(string $message, int $httpCode = 400): void
    {
        response()
            ->httpCode(code: $httpCode)
            ->json(
                value: [
                    'status'  => self::ERROR,
                    'message' => $message,
                ]
            );
    }

    public static function response(?array $data = null, int $httpCode = 200): void
    {
        if (self::statusByHttpCode($httpCode) === self::SUCCESS) {
            self::success(data: $data);
        } else {
            self::error(message: $data['message'], httpCode: $httpCode);
        }
    }
}
