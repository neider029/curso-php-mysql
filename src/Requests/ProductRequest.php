<?php

declare(strict_types = 1);

namespace App\Requests;

use App\Rules\{
    MaxRule, MinRule, NumericRule, RequiredRule, UniqueRule,
};

final class ProductRequest extends AbstractRequest
{
    public function __construct(private readonly ?int $id = null)
    {
        parent::__construct();
    }

    protected function rules(): array
    {
        return [
            'name' => [
                new RequiredRule(message: 'El nombre es requerido'),
                new MinRule(length: 3, message: 'El nombre debe tener como mínimo 3 caracteres'),
                new MaxRule(255, message: 'El nombre debe tener como máximo 255 caracteres'),
                new UniqueRule(table: 'products', column: 'name', id: $this->id, message: 'El producto ya existe'),
            ],
            'description' => [
                new RequiredRule(message: 'La descripción es requerida'),
            ],
            'price' => [
                new RequiredRule(message: 'El precio es requerido'),
                new NumericRule(message: 'El precio debe ser numérico'),
            ],
        ];
    }
}
