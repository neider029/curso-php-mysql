## Cómo inicializar el proyecto 🐘
*   **Requisitos**  
    - Tener php 8.0 o superior.
    - Composer version 2.7.6 o superior
    - Docker o una base de datos mysql.
*   **Instalación**  
    - Ejecutar `composer install` para la instalación de los paquetes requeridos por este proyecto.
    
    **DB con Docker**
    - En caso de tener docker puedes crear un contenedor de mysql. 

       ` docker run --name contenedor-curos-php-mysql -p 3306:3306 -e MYSQL_ROOT_PASSWORD=secret -v curso-de-php-mysql:/var/lib/mysql  mysql:latest`
      - Revisa si el contenedor está ejecutándose con `docker ps`, si no está en ejecución, levanta el contenedor con `docker start contenedor-curos-php-mysql` y rectifica otra vez que el contenedor esté funcionando.
      - Ahora copia y pega el archivo .env.example y llámelo .env , si su contenedor lo creó con el comando anterior, no debería cambiar ninguna configuración. De lo contrario, remplaza la password si la cambió y el puerto si también fue cambiado.
      - Cree una db llamada `apirest` desde cualquier administrador de dbs o accediendo al contenedor.

    **DB local sin docker**
      - Haga una instalación de mysql en su computadora.
      - Ahora copia y pega el archivo .env.example y llamelo .env y remplacé los valores que están en el .env , con los datos respectivos de su db local.
      - Luego cree una db llamada `apirest`

   - Inicializar el servidor PHP `php -S localhost:5000 -t=public`.
   - Y listo, eso sería todo para que el proyecto esté funcionando al 100% 🎉
## Temas que aprendí 🛫
*    **phpstan**  
     Encuentra errores en su código sin escribir pruebas. Es de código abierto y gratuito. Y aparte, puedes ir integrando esta herramienta poco a poco, ya que maneja una escala de niveles del 0 al 9, siendo el 0 el menos riguroso.

*   **php-cs-fixer**  
    Corrige el código para seguir los estándares; si desea seguir los estándares de codificación PHP tal como se definen en PSR-1, PSR-2, PSR12, etc. y más reglas para llevar un estándar en escritura de código hay que usar php-cs-fixer.

*   **PDO**     
    En este programa se implementa el patrón  [Singleton](https://refactoring.guru/es/design-patterns/singleton) que nos ayuda a tener una sola instancia de la DB, así sea llamada en varios lugares del aplicativo.

*   **DotEnv**  
    Permite agregar un archivo .env en el proyecto y usarlo globalmente en tu proyecto de php.

*   **Simple-router**  
    Una librería de PHP que brinda un enrutador y con ella podemos administrar las respuestas y los inputs que llegan por los diferentes verbos como post, put, get, etc.

*   **MakeFile**  
    Lo uso para centralizar todos los comandos que pueda llegar a usar. En este proyecto lo usé para centralizar comandos de vendor .  
    
    EJEMPLO:


    `#!/bin/bash`  
      
    `help: _## Muestra los comandos del archivo_`  
     `@echo 'usage: make [target]'`  
     `@echo`  
     `@echo 'targets:'`  
     `@egrep '^(.+)\:\ ##\ (.+)' ${MAKEFILE_LIST} | column -t -c 2 -s ':#'`  
      
    `phpstan: _## Ejecuta PHPStan_`  
     `./vendor/bin/phpstan analyse -c phpstan.neon`  
      
    `code-style: _## Aplica PHP CS Fixer a nuestro proyecto_`  
     `./vendor/bin/php-cs-fixer fix`  
     
    
    Con esto podía ejecutar `make phpstan` y ahorrarme el trabajo de escribir `./vendor/bin/phpstan analyse -c phpstan.neon`. Y lo mismo para los otros comandos.

*   **Autoload del composer**  
    Aprendí a cómo podría agregar un archivo helpers.php en todo el proyecto y no requerir de llamados innecesarios en los archivos que quisiera usar el helper.  
    Para esto usé la clave files dentro de la llave autoload del archivo composer.json.

    Ejemplo  
    `"autoload": {`  
    `"files":[`  
    `"src/helpers.php"`  
    `]`  
    `},`

*   **PSR-4**  
    Aparte aprendí un poco de los namespace y de psr-4, que es una especificación usada en composer para la carga automática de clases desde un archivo.

    Ejemplo  
    `"autoload": {`  
    `"psr-4": {`  
    `"App\\": "src/"`  
    `},`  
    `},`

*   **Entities**  
    Fabrique una entidad de un producto con el fin de usarla en el controlador y poder guardar, registrar, editar un producto, solo con el llamado de la entidad.

*   **Temas de poo**  
    Aprendí sobre clases abstractas, jerarquía de palabras claves como public,protected,private. Palabras claves como (final) , parent::__construct();