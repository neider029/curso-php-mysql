<?php

declare(strict_types=1);

use Pecee\SimpleRouter\SimpleRouter as Router;

require __DIR__ . '/../vendor/autoload.php';

require __DIR__ . '/../routes/api.php';

$dotenv = Dotenv\Dotenv::createImmutable(paths: __DIR__ . '/../');
$dotenv->load();

Router::setDefaultNamespace(defaultNamespace: '\App\Controllers\Api');
try {
    Router::start();
} catch (\Pecee\Http\Middleware\Exceptions\TokenMismatchException $e) {
    echo 'TokenMismatchException: ' . $e->getMessage();
} catch (\Pecee\SimpleRouter\Exceptions\NotFoundHttpException $e) {
    echo 'NotFoundHttpException: ' . $e->getMessage();
} catch (\Pecee\SimpleRouter\Exceptions\HttpException $e) {
    echo 'HttpException: ' . $e->getMessage();
} catch (Exception $e) {
    echo 'Exception: ' . $e->getMessage();
}